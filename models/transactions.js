const Sequelize = require('sequelize');
const db = require('../db');

module.exports = db.define('transactions', {
  name: Sequelize.STRING,
  amount: Sequelize.FLOAT
});
