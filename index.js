const express = require('express');
const bodyParser = require('body-parser');
const Transactions = require('./models/transactions');
const app = express();

app.use(bodyParser.json());

app.get('/transactions', (req, res) => {
  Transactions.findAll({}).then(transactions => {
    res.json(transactions);
  });
});

app.post('/transactions', (req, res) => {
  Transactions.create(req.body).then(() => {
    res.sendStatus(204);
  });
});

app.listen(8080, () => {
  console.log('server ready at: localhost:8080');
})
